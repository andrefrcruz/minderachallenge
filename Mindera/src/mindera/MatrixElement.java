/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mindera;

/**
 *
 * @author andrecruz
 */
public class MatrixElement {

    private final int line;
    private final int column;

    /**
     * Element constructor
     * @param line
     * @param column
     */
    public MatrixElement(int line, int column) {
        this.line = line;
        this.column = column;
    }
    

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb.append("[").append(line).append(",").append(column).append("]").toString();
        
    }
    
}
