/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mindera;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author andrecruz
 */
public class Write {

    /**
     * Method to write to ouput file
     * @param outputFile
     * @param str
     */
    public static void writeToFile(BufferedWriter outputFile, String str) {

        try {
            outputFile.append(str);
        } catch (IOException e) {
        }
    }

    /**
     * method to write a new line in output file
     * @param outputFile
     */
    public static void writeNewLine(BufferedWriter outputFile) {

        try {
            outputFile.newLine();
        } catch (IOException e) {
        }
    }

    /**
     * Method to create new output file.
     * @param newOuputFile
     * @return BufferedWriter
     */
    public static BufferedWriter createNewOuputFile(String newOuputFile) {

        BufferedWriter bufferedFile = null;
        File file = new File(newOuputFile);

        if (file.exists()) {
            file.delete();
        }

        try {
            File newFile = new File(newOuputFile);
            bufferedFile = new BufferedWriter(new FileWriter(newFile, true),1024);
            bufferedFile.append("[");
            bufferedFile.newLine();
        } catch (IOException e) {
        }
        return bufferedFile;
    }

    /**
     * method to close the openned output file.
     * @param outputFile
     */
    public static void closeOutputFile(BufferedWriter outputFile) {
        try {
            if (outputFile != null) {
                outputFile.append("]");
                outputFile.close();
            }
        } catch (IOException e) {
        }
    }
    
}
