/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mindera;

import java.io.BufferedWriter;

/**
 *
 * @author André
 */
public class DataProcess {

    /**
     * Method to find islands in matrix.
     *
     * @param buffer
     */
    public static void collectIsland(BufferedWriter buffer) {
        for (int i = 0; i < Mindera.matrix.length; i++) {
            for (int j = 0; j < Mindera.matrix[0].length; j++) {
                
                if (Mindera.matrix[i][j] == '1') {
                    
                    Island island = new Island();
                    
                    checkIslandExistance(i, j, Mindera.matrix, island);
                    
                    if (island.getListElements().size() > 1) {
                        Write.writeToFile(buffer, island.toString());
                        Write.writeNewLine(buffer);
                    }
                    
                }
            }
        }
    }

    /**
     * Recursive method to check if island exists
     * 
     * @param i
     * @param j
     * @param matrix
     * @param island
     */
    public static void checkIslandExistance(int i, int j, char[][] matrix, Island island) {
        
        matrix[i][j] = '0';
        
        island.getListElements().add(new MatrixElement(i, j));
        
        if (i - 1 >= 0 && Mindera.matrix[i-1][j] == '1') {
            checkIslandExistance(i - 1, j, matrix, island);
        }
        
        if (i + 1 < Mindera.matrix[0].length && Mindera.matrix[i+1][j] == '1') {
            checkIslandExistance(i + 1, j, matrix, island);
        }
        
        if (j - 1 >= 0 && Mindera.matrix[i][j-1] == '1') {
            checkIslandExistance(i, j - 1, matrix, island);
        }
        
        if (j + 1 < Mindera.matrix.length && Mindera.matrix[i][j+1] == '1') {
            checkIslandExistance(i, j + 1, matrix, island);
        }
        
    }
    
}
