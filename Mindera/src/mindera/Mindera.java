/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mindera;

import java.io.BufferedWriter;
import java.util.Scanner;

/**
 *
 * @author andrecruz
 */
public class Mindera {

    private static String SOURCEFILE;
    private static final String OUTPUTFILE = "src/sourceFiles/outputFile.txt";
    protected static char[][] matrix;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.printf("Enter file name: ");
        SOURCEFILE = "src/sourceFiles/" + scanner.nextLine() + ".json";
        System.out.printf("Enter number of rows: ");
        int numberOfRows = scanner.nextInt();
        System.out.printf("Enter number of columns: ");
        int numberOfColumns = scanner.nextInt();

        matrix = new char[numberOfRows][numberOfColumns];

        final long startReadFile = System.nanoTime();

        BufferedWriter newBuffer = Write.createNewOuputFile(OUTPUTFILE);

        Read.readFile(SOURCEFILE, matrix);

        DataProcess.collectIsland(newBuffer);

        final long endReadFile = System.nanoTime();
        String readFromFile = ("\n1st -> execution process " + "duration: "
                + (((endReadFile - startReadFile) / 10000000) * (0.01)) + "sec");

        System.out.println(readFromFile);
        Write.closeOutputFile(newBuffer);
    }

}
