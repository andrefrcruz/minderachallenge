/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mindera;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author andrecruz
 */
public class Read {

    /**
     * Method to read from the source file and fill the new matrix.
     *
     * @param sourceFile
     * @param matrix
     */
    public static void readFile(String sourceFile, char[][] matrix) {
        BufferedReader bufferedreader = null;
        FileReader fileReader = null;

        try {

            fileReader = new FileReader(sourceFile);
            bufferedreader = new BufferedReader(fileReader);

            String sCurrentLine;

            int i = 0;

            bufferedreader.readLine();

            while ((sCurrentLine = bufferedreader.readLine()) != null) {
                int column = 0;
                if (i == Mindera.matrix.length - 1) {

                    for (int j = 1; j < sCurrentLine.length() - 1; j += 2) {
                        Mindera.matrix[i][column] = sCurrentLine.charAt(j);
                        column++;
                    }
                } else {
                    for (int j = 1; j < sCurrentLine.length() - 2; j += 2) {
                        Mindera.matrix[i][column] = sCurrentLine.charAt(j);
                        column++;
                    }
                    i++;
                }
            }

        } catch (IOException e) {
        } finally {
            try {
                if (bufferedreader != null) {
                    bufferedreader.close();
                }
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException ex) {
            }

        }

    }
}
