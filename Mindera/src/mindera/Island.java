/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mindera;

import java.util.ArrayList;

/**
 *
 * @author andrecruz
 */
public class Island {

    private final ArrayList<MatrixElement> listElements;

    public Island() {
        this.listElements = new ArrayList<>();
    }

    public ArrayList<MatrixElement> getListElements() {
        return listElements;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        listElements.forEach((e) -> {
            sb.append(e.toString());
        });
        sb.append("]");
        return sb.toString();
    }

}
